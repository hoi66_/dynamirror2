package me.armin.dynamirror.main;

import me.armin.dynamirror.spotify.Refresh;
import me.armin.dynamirror.ui.Interface;

public class BootSequencer {
	public static void main(String[] args) {
		FrameSequence.displaySwitchFrame();
		Refresh.authorizationCodeRefresh_Async();
		Interface.startMaleUI();
		
	}

}

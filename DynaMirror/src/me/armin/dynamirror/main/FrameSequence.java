package me.armin.dynamirror.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class FrameSequence implements Runnable {

	public static void displaySwitchFrame() {
		Runnable runnable = new FrameSequence();
		Thread thread = new Thread(runnable);
		thread.start();
	}

	@Override
	public void run() {
		JFrame frame = new JFrame("placeholder");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel textLabel = new JLabel("", SwingConstants.CENTER);
		textLabel.setPreferredSize(new Dimension(4000, 4000));
		frame.getContentPane().add(textLabel, BorderLayout.CENTER);
		frame.getContentPane().setBackground(Color.black);
		frame.setUndecorated(true);
		// Display the window.
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}
}

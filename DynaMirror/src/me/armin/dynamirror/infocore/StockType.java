package me.armin.dynamirror.infocore;

public enum StockType {
	SSMI,
	CHFUSD,
	CHFGBP,
	CHFEUR,
	TESLA,
	INTEL,
	APPLE,
	AMAZON,
	UBS,
	CS,
	GOOGLE,
	BAYER,
	SIEMENS,
	GLENCORE,
	ROCHE,
	NOVATIS,
	NESTLE
	

}

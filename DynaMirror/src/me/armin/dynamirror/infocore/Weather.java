package me.armin.dynamirror.infocore;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Weather {
	private static String rtemp = "?";
	private static String weekDay = "?";

	public static String getTemperatureCurrentDay() {
		Document doc;
		try {
			doc = Jsoup.connect("http://www.wetter.ch/de/Schweiz/Solothurn/2658564/").get();

			Element content = doc.getElementById("main");
			Elements temp = content.getElementsByClass("temperature");
			rtemp = temp.text();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rtemp = "?";

		}
		return rtemp;

	}

	public static String getWeekDayForDay(int day) {
		String[] splitted = { "" };
		Document doc;
		try {
			doc = Jsoup.connect("http://www.wetter.ch/de/Schweiz/Solothurn/2658564/").get();

			Element content = doc.getElementById("main");
			Elements temp = content.getElementsByClass("forecast");
			splitted = temp.text().split(" ");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			weekDay = "?";
		}
		if (day == 1) {
			weekDay = "Heute";
		} else if (day == 2) {
			weekDay = "Morgen";
		} else if (day == 3) {
			weekDay = splitted[9];
		} else if (day == 4) {
			weekDay = splitted[12];
		} else if (day == 5) {
			weekDay = splitted[15];
		} else if (day == 6) {
			weekDay = splitted[18];
		} else if (day == 7) {
			weekDay = splitted[21];
		} else {
			weekDay = "Error";
		}
		return weekDay;

	}

	public static String getTemperatureLevelsForDay(int day) {
		String[] splitted = { "" };
		String tempLevels = "?";
		Document doc;
		try {
			doc = Jsoup.connect("http://www.wetter.ch/de/Schweiz/Solothurn/2658564/").get();

			Element content = doc.getElementById("main");
			Elements temp = content.getElementsByClass("forecast");
			String[] filter = { "Wetteraussichten", "Tagesprognose", "Heute", "recht", "sonnig", "freundlich",
					"wechselnd", "bew�lkt", "Regen", "Wolkenfelder", "kaum", "meist", "klar", "unbew�lkt", "tr�b",
					"nass", "starker", "stark", "Flocken", "Schneeschauer", "etwas", ",", "Montag", "Dienstag",
					"Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag", "Morgen", "Langzeitprognose",
					"Schneeregenschauer", "gewitterhaft", "Schneeregen", "und" };
			String tempInfo = temp.text();
			for (int i = 0; i < filter.length; ++i) {
				tempInfo = tempInfo.replace(filter[i], "");
			}
			tempInfo = tempInfo.replaceAll("\\s+", " ");
			String info = tempInfo.trim().replace(" +", " ");
			splitted = info.split(" ");
			if (day == 1) {
				tempLevels = splitted[16] + "/" + splitted[15];
			} else if (day == 2) {
				tempLevels = splitted[18] + "/" + splitted[17];
			} else if (day == 3) {
				tempLevels = splitted[20] + "/" + splitted[19];
			} else if (day == 4) {
				tempLevels = splitted[22] + "/" + splitted[21];
			} else if (day == 5) {
				tempLevels = splitted[24] + "/" + splitted[23];
			} else if (day == 6) {
				tempLevels = splitted[26] + "/" + splitted[25];
			} else if (day == 7) {
				tempLevels = splitted[28] + "/" + splitted[27];
			} else {
				tempLevels = "Error";
			}
			System.out.println(tempLevels);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rtemp = "?";
		}
		return tempLevels;

	}

	public static String getCurrentWeatherDescription() {
		Document doc;
		try {
			doc = Jsoup.connect("http://www.wetter.ch/de/Schweiz/Solothurn/2658564/").get();

			Element content = doc.getElementById("main");
			Element temp = content.getElementsByClass("text").first();
			System.out.println(temp.text());
			return temp.text();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			weekDay = "?";
		}
		return "yesn't";

	}

	public static int getMaxTempForCurrentDay() {
		Document doc;
		int maxTemp;
		try {
			doc = Jsoup.connect("http://www.wetter.ch/de/Schweiz/Solothurn/2658564/").get();

			Element content = doc.getElementById("main");
			Element temp = content.getElementsByClass("longforecast_tempmax").first();
			return maxTemp = Integer.parseInt(temp.text().replace("�C", ""));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			weekDay = "?";
		}
		return 0;

	}
	public static String getSVGPath() {
		Document doc;
		int maxTemp;
		try {
			doc = Jsoup.connect("http://media3.news.ch/img/wetter/symb0.0_newsch/SVG/55.svg").ignoreContentType(true).get();

			Elements content = doc.getElementsByTag("g");
			System.out.println(content);
			//Element temp = content.getElementsByClass("longforecast_tempmax").first();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			weekDay = "?";
		}
		return null;
		
	}

}

package me.armin.dynamirror.infocore;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Moon {
	private static String moonDate = "?";
	public static String getNextFullMoon() {
		Document doc;
		String[] splitted;
		try {
			doc = Jsoup.connect("https://www.timeanddate.de/mond/phasen/schweiz/solothurn").get();
			Element links = doc.getElementById("mn-cyc");
			Element moonTxt = links.getElementsByTag("td").get(11);
			splitted = moonTxt.text().split(" ");
			moonDate = "N�chster Vollmond:\n" + splitted[0] + " " + splitted[1] + " um " + splitted[2];
			System.out.println(moonDate);
		} catch (IOException e) {
			doc = null;
			moonDate = "Fehler";
			e.printStackTrace();
		
		}
		return moonDate;
	}

}

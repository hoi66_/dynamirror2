package me.armin.dynamirror.infocore;

import javafx.scene.paint.Color;

public class FAUtilities {
	public static String[] teHSun = { "Heute ist es sehr warm, wie w�re es mit etwas farbenfrohem und leichtem?",
			"Ein heisser Tag steht bevor! Zeit f�r leichte Kleider mit knalligeren Farben!", 
			"Sonniger Tag, sonnige Freude! Diese Kombination w�re perfekt f�r heute!"};
	public static String[] teHNeu = {"Angenehmes Wetter, ausgeglichene Farben und leichte Kleider sind ideal!,"
			+ "Hm, warmes Wetter, vielleicht etwas auff�lligere Farben und bequeme Kleider?",
			"Warme Temperaturen! Dir w�rden auff�lligere Farben kombiniert mit sommerlichen Kleidern stehen!"};
	public static String[] teHBad = {"Hm... warm aber nass. Weniger auff�llige Farben, sommerliche Kleider mit Regenschutz, das ist es!"};
	public static String[] teNSun = { "Heute ist ein sch�ner Tag! Probier es mit auff�lligen Farben und nicht zu warmen Kleidern!"};
	public static String[] teNNeu = { "Etwas gleichg�ltig das Wetter heute... meide auff�llige Kleider und zieh dich etwas w�rmer an." };
	public static String[] teNBad = { "Das Wetter hat es heute nicht so, d�nklere Farben und entsprechend warme Kleider sind angemessen" };
 	public static String[] teLSun = { "Kalter und sonniger Tag, fr�hliche Farben schaden nicht, aber zieh dich warm an!" };
 	public static String[] teLNeu = { "Brrr, was ein kalter Tag! Zieh warme Kleider an, vielleicht nicht allzu auff�llige Farben?" };
 	public static String[] teLBad = { "Niederschlag und dazu noch K�lte! Sehr warme Kleider sind von Vorteil. Schau dir vielleicht diese Farben an:  " };
	

	public static String rainWarning1 = "Es k�nnte in der n�chsten Zeit regnen, nimm eventuell einen Schirm mit.";
	public static String rainWarning2 = "Starker Regen f�r heute gemeldet, Regenjacke eventuell von Vorteil.";
	public static String rainWarning3 = "Gewitter gemeldet, nimm entsprechenden Schutz mit!";			
	
	public static String increasedRainChance = "Sehr Wahrscheinlich kommt es zu Niederschlag, nimm Regenschutz mit.";

	public static Color[] sunnyColor1 = {Color.LIGHTSKYBLUE, Color.PALEVIOLETRED, Color.BLUEVIOLET, Color.CORNFLOWERBLUE, Color.LIGHTPINK, Color.ROYALBLUE };
	public static Color[] sunnyColor2 = {Color.YELLOW, Color.FLORALWHITE, Color.FLORALWHITE, Color.GHOSTWHITE, Color.AQUA, Color.WHITE};
	public static Color[] neutralColor1 = {Color.BLUE, Color.DARKCYAN, Color.CRIMSON };
	public static Color[] neutralColor2 = {Color.LIGHTSLATEGRAY, Color.ANTIQUEWHITE, Color.FLORALWHITE};
	public static Color[] darkerColor1 = {Color.MEDIUMBLUE, Color.DARKOLIVEGREEN, Color.SILVER, Color.color(0.1, 0.26, 0.77), Color.DARKRED };
	public static Color[] darkerColor2 = {Color.DIMGRAY, Color.GRAY, Color.SILVER, Color.GREY, Color.color(0.34, 0.48, 0.63) };
	
	public static String[] sunnyHairstyle = { "Ich finde, dass Du deine Haare zu einem Dutt machen solltest. Es ist sch�nes Wetter!", "Bei diesem Wetter w�re ein Pferdeschwanz besonders geeignet. Versuch es doch mal!", "Wie w�re es mit einem Haarzopf? Das w�rde bestimmt wundersch�n aussehen!" };
	public static String[] neutralHairstyle = { "Ich finde, dass du deine Haar heute lieber offen lassen solltest." };
	public static String[] badWeatherHairstyle = { "Bei dem Wetter w�ren gebundene Haare eher Vorteilhaft. �berlegs dir gut! ", "Offene Haare nicht zu empfehlen. Wie w�re es mit einem gebundenen Haarstyle?" };
	
	public static String[] sunnyMakeup = { "Zeig dich von deiner sch�nen Seite! Make Up schadet heute nicht. Wie w�re es mit einem etwas verr�ckterem Style und auff�lligem Aussehen?" };
	public static String[] neutralMakeup = { "Make Up? Warum auch nicht! Probier es heute mit etwas neutraleren oder gar d�nkleren Farben aus. Schadet bestimmt nicht und passt zum Tag!" };
	public static String[] badWeatherMakeup = { "Meide heute Make Up, bei diesem Wetter ist es wirklich nicht zu empfehlen." };
	
	
	
	}             
                           
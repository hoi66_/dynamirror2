package me.armin.dynamirror.infocore;

import java.io.IOException;
import java.math.BigDecimal;
import me.armin.dynamirror.infocore.StockType;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.quotes.fx.FxQuote;
public class Stocks {

	public static BigDecimal getStock(StockType stock) {
		try {
			if (stock == StockType.CHFEUR) {
				FxQuote qeur = YahooFinance.getFx("CHFEUR=X");
				return qeur.getPrice();
			} else if (stock.equals(StockType.CHFGBP)) {
				FxQuote qgbp = YahooFinance.getFx("CHFGBP=X");
				return qgbp.getPrice();
			} else if (stock.equals(StockType.CHFUSD)) {
				FxQuote qusd = YahooFinance.getFx("CHFUSD=X");
				return qusd.getPrice();
			} else if (stock.equals(StockType.SSMI)) {
				Stock swissm = YahooFinance.get("^SSMI");
				return swissm.getQuote().getPrice();
			} else if (stock.equals(StockType.AMAZON)) {
				Stock amzn = YahooFinance.get("AMZN");
				return amzn.getQuote().getPrice();
			} else if (stock.equals(StockType.APPLE)) {
				Stock aapl = YahooFinance.get("AAPL");
				return aapl.getQuote().getPrice();
			} else if (stock.equals(StockType.BAYER)) {
				Stock bayn = YahooFinance.get("BAYN");
				return bayn.getQuote().getPrice();
			} else if (stock.equals(StockType.CS)) {
				Stock cs = YahooFinance.get("CS");
				return cs.getQuote().getPrice();
			} else if (stock.equals(StockType.GLENCORE)) {
				Stock glen = YahooFinance.get("GLEN.L");
				return glen.getQuote().getPrice();
			} else if (stock.equals(StockType.GOOGLE)) {
				Stock googl = YahooFinance.get("GOOGL");
				return googl.getQuote().getPrice();
			} else if (stock.equals(StockType.INTEL)) {
				Stock intc = YahooFinance.get("INTC");
				return intc.getQuote().getPrice();
			} else if (stock.equals(StockType.NOVATIS)) {
				Stock novn = YahooFinance.get("NOVN");
				return novn.getQuote().getPrice();
			} else if (stock.equals(StockType.ROCHE)) {
				Stock rog = YahooFinance.get("ROG");
				return rog.getQuote().getPrice();
			} else if (stock.equals(StockType.SIEMENS)) {
				Stock sin = YahooFinance.get("SIN");
				return sin.getQuote().getPrice();
			} else if (stock.equals(StockType.TESLA)) {
				Stock tsla = YahooFinance.get("TSLA");
				return tsla.getQuote().getPrice();
			} else if (stock.equals(StockType.UBS)) {
				Stock ubsg = YahooFinance.get("UBSG.VX");
				return ubsg.getQuote().getPrice();
			} else if (stock.equals(StockType.NESTLE)) {
				Stock ubsg = YahooFinance.get("NESN.VX");
				return ubsg.getQuote().getPrice();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}	
	public static BigDecimal getStockPercentage(StockType stock) {
		try {
			if (stock.equals(StockType.SSMI)) {
				Stock swissm = YahooFinance.get("^SSMI");
				return swissm.getQuote().getChange();
			} else if (stock.equals(StockType.AMAZON)) {
				Stock amzn = YahooFinance.get("AMZN");
				return amzn.getQuote().getChange();
			} else if (stock.equals(StockType.APPLE)) {
				Stock aapl = YahooFinance.get("AAPL");
				return aapl.getQuote().getChange();
			} else if (stock.equals(StockType.BAYER)) {
				Stock bayn = YahooFinance.get("BAYN");
				return bayn.getQuote().getChange();
			} else if (stock.equals(StockType.CS)) {
				Stock cs = YahooFinance.get("CS");
				return cs.getQuote().getChange();
			} else if (stock.equals(StockType.GLENCORE)) {
				Stock glen = YahooFinance.get("GLEN.L");
				return glen.getQuote().getChange();
			} else if (stock.equals(StockType.GOOGLE)) {
				Stock googl = YahooFinance.get("GOOGL");
				return googl.getQuote().getChange();
			} else if (stock.equals(StockType.INTEL)) {
				Stock intc = YahooFinance.get("INTC");
				return intc.getQuote().getChange();
			} else if (stock.equals(StockType.NOVATIS)) {
				Stock novn = YahooFinance.get("NOVN");
				return novn.getQuote().getChange();
			} else if (stock.equals(StockType.ROCHE)) {
				Stock rog = YahooFinance.get("ROG");
				return rog.getQuote().getChange();
			} else if (stock.equals(StockType.SIEMENS)) {
				Stock sin = YahooFinance.get("SIN");
				return sin.getQuote().getChange();
			} else if (stock.equals(StockType.TESLA)) {
				Stock tsla = YahooFinance.get("TSLA");
				return tsla.getQuote().getChange();
			} else if (stock.equals(StockType.UBS)) {
				Stock ubsg = YahooFinance.get("UBSG.VX");
				return ubsg.getQuote().getChange();
			} else if (stock.equals(StockType.NESTLE)) {
				Stock ubsg = YahooFinance.get("NESN.VX");
				return ubsg.getQuote().getChange();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}


}

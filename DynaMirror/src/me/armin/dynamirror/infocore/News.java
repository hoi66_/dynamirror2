package me.armin.dynamirror.infocore;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class News {
	
	public static String getHeadline(int article) {
		Document doc;
		String txt; 
		try {
			doc = Jsoup.connect("https://www.srf.ch/").get();
			Element links = doc.getElementsByClass("teaser__title").get(article);
			txt = links.text();
		} catch (IOException e) {
			doc = null;
			e.printStackTrace();
			txt = "Ein Fehler ist aufgetreten";
		
		}
		return txt;
	}
	public static String getTeaserLine(int article) {
		Document doc;
		String txt; 
		try {
			doc = Jsoup.connect("https://www.srf.ch/").get();
			Element links = doc.getElementsByClass("teaser__lead").get(article);
			txt = links.text();
			System.out.println(txt);
		} catch (IOException e) {
			doc = null;
			e.printStackTrace();
			txt = "Ein Fehler ist aufgetreten";
		
		}
		return null;
		
	}

}

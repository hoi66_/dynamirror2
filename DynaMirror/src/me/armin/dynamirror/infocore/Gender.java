package me.armin.dynamirror.infocore;

public enum Gender {
	MALE,
	FEMALE,
	NOT_RECOGNIZED

}

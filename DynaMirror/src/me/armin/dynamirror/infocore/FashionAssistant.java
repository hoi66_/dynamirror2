package me.armin.dynamirror.infocore;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;
import me.armin.dynamirror.infocore.TemperatureLevel;
import javafx.scene.paint.Color;

public class FashionAssistant {
	private static Calendar calendar = Calendar.getInstance();
	private static int dayCached;
	private static String[] goodWeather = { "sonnig", "unbew�lkt", "klar" };
	private static String[] neutralWeather = { "freundlich", "bew�lkt", "Wolkenfelder" };
	private static String[] badWeather = { "Schneeregenschauer", "tr�b", "Schneeregen", "gewitterhaft", "Regen",
			"nass" };
	private static String[] rainProbability1 = { "wechselnd", "bew�lkt", "Wolkenfelder" };
	private static String[] rainProbability2 = { "Regen", "Schneeregenschauer", "Schneeregen", "tr�b", "nass" };
	private static String[] rainIntensifier = { "stark", "starker" };
	private static Color color1;
	private static Color color2;

	public static Color getColor1() {
		return color1;
	}

	public static Color getColor2() {
		return color2;
	}

	public static String[] getFashionSuggestion() {
		FashionAssistant fa = new FashionAssistant();
		Random r = new Random();
		int randomText;
		int randomColorCombo;
		String[] returnedStringCombo = { "", "", "", "" };
		if (stringContainsItemFromList(Weather.getCurrentWeatherDescription(), goodWeather)) {
			if (getTemperatureCategory().equals(TemperatureLevel.HIGH)) {
				randomColorCombo = r.nextInt(FAUtilities.sunnyColor1.length);
				color1 = FAUtilities.sunnyColor1[randomColorCombo];
				color2 = FAUtilities.sunnyColor2[randomColorCombo];

				randomText = r.nextInt(FAUtilities.teHSun.length);
				returnedStringCombo[0] = FAUtilities.teHSun[randomText];
				returnedStringCombo[1] = fa.checkForRainWarning();
				returnedStringCombo[2] = fa.getHairSuggestion(WeatherType.GOOD);
				returnedStringCombo[3] = FAUtilities.sunnyMakeup[0];
				return returnedStringCombo;

			} else if (getTemperatureCategory().equals(TemperatureLevel.MID)) {
				randomColorCombo = r.nextInt(FAUtilities.neutralColor1.length);
				color1 = FAUtilities.neutralColor1[randomColorCombo];
				color2 = FAUtilities.neutralColor2[randomColorCombo];

				randomText = r.nextInt(FAUtilities.teNSun.length);
				returnedStringCombo[0] = FAUtilities.teNSun[randomText];
				returnedStringCombo[1] = fa.checkForRainWarning();
				returnedStringCombo[2] = fa.getHairSuggestion(WeatherType.GOOD);
				returnedStringCombo[3] = FAUtilities.sunnyMakeup[0];
				return returnedStringCombo;
			} else if (getTemperatureCategory().equals(TemperatureLevel.LOW)) {
				randomColorCombo = r.nextInt(FAUtilities.darkerColor1.length);
				color1 = FAUtilities.darkerColor1[randomColorCombo];
				color2 = FAUtilities.darkerColor2[randomColorCombo];
				returnedStringCombo[2] = fa.getHairSuggestion(WeatherType.GOOD);

			}
		} else if (stringContainsItemFromList(Weather.getCurrentWeatherDescription(), neutralWeather)) {

			if (getTemperatureCategory().equals(TemperatureLevel.HIGH)) {
				randomColorCombo = r.nextInt(FAUtilities.sunnyColor1.length);
				color1 = FAUtilities.sunnyColor1[randomColorCombo];
				color2 = FAUtilities.sunnyColor2[randomColorCombo];

				randomText = r.nextInt(FAUtilities.teHNeu.length);
				returnedStringCombo[0] = FAUtilities.teHNeu[randomText];
				returnedStringCombo[1] = fa.checkForRainWarning();
				returnedStringCombo[2] = fa.getHairSuggestion(WeatherType.NEUTRAL);
				returnedStringCombo[3] = FAUtilities.sunnyMakeup[0];
				return returnedStringCombo;
			} else if (getTemperatureCategory().equals(TemperatureLevel.MID)) {
				randomColorCombo = r.nextInt(FAUtilities.neutralColor1.length);
				color1 = FAUtilities.neutralColor1[randomColorCombo];
				color2 = FAUtilities.neutralColor2[randomColorCombo];

				randomText = r.nextInt(FAUtilities.teNNeu.length);
				returnedStringCombo[0] = FAUtilities.teNNeu[randomText];
				returnedStringCombo[1] = fa.checkForRainWarning();
				returnedStringCombo[2] = fa.getHairSuggestion(WeatherType.NEUTRAL);
				return returnedStringCombo;
			} else if (getTemperatureCategory().equals(TemperatureLevel.LOW)) {
				randomColorCombo = r.nextInt(FAUtilities.darkerColor1.length);
				color1 = FAUtilities.darkerColor1[randomColorCombo];
				color2 = FAUtilities.darkerColor2[randomColorCombo];

				randomText = r.nextInt(FAUtilities.teLNeu.length);
				returnedStringCombo[0] = FAUtilities.teLNeu[randomText];
				returnedStringCombo[1] = fa.checkForRainWarning();
				returnedStringCombo[2] = fa.getHairSuggestion(WeatherType.NEUTRAL);
				returnedStringCombo[3] = FAUtilities.badWeatherMakeup[0];
				return returnedStringCombo;
			}

		} else if (stringContainsItemFromList(Weather.getCurrentWeatherDescription(), badWeather)) {
			if (getTemperatureCategory().equals(TemperatureLevel.HIGH)) {
				randomColorCombo = r.nextInt(FAUtilities.sunnyColor1.length);
				color1 = FAUtilities.sunnyColor1[randomColorCombo];
				color2 = FAUtilities.sunnyColor2[randomColorCombo];
				returnedStringCombo[3] = FAUtilities.neutralMakeup[0];

				randomText = r.nextInt(FAUtilities.teHBad.length);
				returnedStringCombo[0] = FAUtilities.teHBad[randomText];
				returnedStringCombo[1] = fa.checkForRainWarning();
				returnedStringCombo[2] = fa.getHairSuggestion(WeatherType.BAD);
				return returnedStringCombo;
			} else if (getTemperatureCategory().equals(TemperatureLevel.MID)) {
				randomColorCombo = r.nextInt(FAUtilities.neutralColor1.length);
				color1 = FAUtilities.neutralColor1[randomColorCombo];
				color2 = FAUtilities.neutralColor2[randomColorCombo];
				returnedStringCombo[3] = FAUtilities.badWeatherMakeup[0];
				returnedStringCombo[2] = fa.getHairSuggestion(WeatherType.BAD);
			} else if (getTemperatureCategory().equals(TemperatureLevel.LOW)) {
				randomColorCombo = r.nextInt(FAUtilities.darkerColor1.length);
				color1 = FAUtilities.darkerColor1[randomColorCombo];
				color2 = FAUtilities.darkerColor2[randomColorCombo];
				returnedStringCombo[3] = FAUtilities.badWeatherHairstyle[0];
				randomText = r.nextInt(FAUtilities.teLBad.length);
				returnedStringCombo[0] = FAUtilities.teLBad[randomText];
				returnedStringCombo[1] = fa.checkForRainWarning();
				returnedStringCombo[2] = fa.getHairSuggestion(WeatherType.BAD);
				return returnedStringCombo;
			}

		}

		return returnedStringCombo;

	}

	public void cacheDay() {
		int dayc = calendar.get(Calendar.DAY_OF_WEEK);
		dayc = dayCached;
	}

	private static boolean stringContainsItemFromList(String input, String[] items) {
		return Arrays.stream(items).parallel().anyMatch(input::contains);
	}

	private static TemperatureLevel getTemperatureCategory() {
		if (Weather.getMaxTempForCurrentDay() < 11) {
			return TemperatureLevel.LOW;
		} else if (Weather.getMaxTempForCurrentDay() > 11 && Weather.getMaxTempForCurrentDay() < 26)
			return TemperatureLevel.MID;
		else if (Weather.getMaxTempForCurrentDay() > 25) {
			return TemperatureLevel.HIGH;
		}

		return null;

	}

	public String getHairSuggestion(WeatherType weather) {
		Random r = new Random();
		int randomHairstyle;
		if (weather == WeatherType.GOOD) {
			randomHairstyle = r.nextInt(FAUtilities.sunnyHairstyle.length);
			return FAUtilities.sunnyHairstyle[randomHairstyle];
		} else if (weather == WeatherType.NEUTRAL) {
			randomHairstyle = r.nextInt(FAUtilities.sunnyHairstyle.length);
			return FAUtilities.sunnyHairstyle[randomHairstyle];
		} else if (weather == WeatherType.BAD) {
			randomHairstyle = r.nextInt(FAUtilities.sunnyHairstyle.length);
			return FAUtilities.sunnyHairstyle[randomHairstyle];
		}

		return "Fehler";

	}

	public String checkForRainWarning() {
		String desc = Weather.getCurrentWeatherDescription();
		if (stringContainsItemFromList(desc, rainProbability1) && stringContainsItemFromList(desc, rainIntensifier)) {
			return FAUtilities.increasedRainChance;
		} else if (stringContainsItemFromList(desc, rainProbability2)
				&& stringContainsItemFromList(desc, rainIntensifier)) {
			return FAUtilities.rainWarning3;
		} else if (stringContainsItemFromList(desc, rainProbability1)) {
			return FAUtilities.rainWarning1;
		} else if (stringContainsItemFromList(desc, rainProbability2)) {
			return FAUtilities.rainWarning2;
		} else {
			return null;

		}
	}

	private enum WeatherType {
		GOOD, NEUTRAL, BAD
	}
}

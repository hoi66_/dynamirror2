package me.armin.dynamirror.infocore;

import java.io.IOException;
import java.net.UnknownHostException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Quote  {
	public static String quoteOfTheDay;

	public static String getQuote() throws UnknownHostException {
		Document doc;
		try {
			doc = Jsoup.connect("https://www.spruch-des-tages.org/").get();
			Element links = doc.getElementsByClass("h2").first();
			quoteOfTheDay = links.text();
		} catch (IOException e) {
			doc = null;
			quoteOfTheDay = "Fehler beim Laden des Spruchs";
			e.printStackTrace();
		
		}
		return quoteOfTheDay;
	}

}

package me.armin.dynamirror.infocore;

import java.io.IOException;

import javafx.scene.image.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Horoscope {
	public static String getHoroscopeForSymbol(HoroscopeSymbol symbol) {
		String htext = "?";
		String[] splitted = { "" };
		Document doc;
		String bridge;
		try {
			System.out.println(symbol.toString());
			doc = Jsoup.connect("https://www.20min.ch/leben/horoskop/").get();

			Element content = doc.getElementById("content");
			Elements temp = content.getElementsByClass("sign " + symbol.toString().toLowerCase());
			Element tempn = temp.select("p").get(1);
			splitted = tempn.text().split("(?<=[.?!])\\s+(?=[\\D\\d])");
			bridge = splitted[0] + " " + splitted[1] + " " + splitted[2];
			
			if (bridge.length() < 330) {
				htext = splitted[0] + " " + splitted[1] + " " + splitted[2];
			} else {
				htext = splitted[0] + " " + splitted[1];
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			htext = "?";
		}

		return htext;
	}

	public static Image getImageForSymbol(HoroscopeSymbol symbol) {
		Image image;
		if (symbol == HoroscopeSymbol.ZWILLING || symbol == HoroscopeSymbol.FISCH) {
			image = new Image(
					"https://php.blick.ch/red/tools/horoskop/img/" + symbol.toString().toLowerCase() + "e.png");
			return image;
		} else {
			image = new Image(
					"https://php.blick.ch/red/tools/horoskop/img/" + symbol.toString().toLowerCase() + ".png");

			return image;
		}

	}
}

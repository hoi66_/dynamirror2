package me.armin.dynamirror.sound;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SoundManager {

	public static void playSound(Enum e) throws LineUnavailableException, UnsupportedAudioFileException, IOException {
		if (e.equals(SOUNDS.BOOTING)) {
			File soundFile = new File("C:\\Users\\famja\\Desktop\\Abschlussprojekt Data\\FadingPingPong.wav");
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);

			Clip clip = AudioSystem.getClip();

			clip.open(audioIn);
			clip.start();
		} else if (e.equals(SOUNDS.MIRROR_READY)) {
			File soundFile = new File("C:\\Users\\famja\\Desktop\\Abschlussprojekt Data\\MirrorBooted.wav");
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);

			Clip clip = AudioSystem.getClip();

			clip.open(audioIn);
			clip.start();
		} else if (e.equals(SOUNDS.RECOGNIZER_BOOTING)) {
			File soundFile = new File("C:\\Users\\famja\\Desktop\\Abschlussprojekt Data\\BootingRecognizer.wav");
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);

			Clip clip = AudioSystem.getClip();

			clip.open(audioIn);
			clip.start();

		} else {
			System.out.println("error");
		}
	}
}
package me.armin.dynamirror.ui;


import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jfxtras.styles.jmetro8.JMetro;
import jfxtras.styles.jmetro8.JMetro.Style;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.fxml.FXMLLoader;

public class Interface extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Interface.fxml"));
			VBox root = (VBox) loader.load();
			Scene scene = new Scene(root, 1920, 1080);
			JMetro metro = new JMetro(Style.DARK);
			metro.applyTheme(root);

			InterfaceController controller = loader.getController();
			controller.setStage(primaryStage);
			controller.init();

			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.initStyle(StageStyle.UNDECORATED);
			primaryStage.setAlwaysOnTop(true);
			primaryStage.setScene(scene);
			primaryStage.setFullScreen(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void startMaleUI() {
		launch();
	}
}

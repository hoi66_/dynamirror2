package me.armin.dynamirror.ui;

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javafx.scene.shape.Circle;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import me.armin.dynamirror.biometric.BioCore;
import me.armin.dynamirror.infocore.FashionAssistant;
import me.armin.dynamirror.infocore.Gender;
import me.armin.dynamirror.infocore.Horoscope;
import me.armin.dynamirror.infocore.HoroscopeSymbol;
import me.armin.dynamirror.infocore.Moon;
import me.armin.dynamirror.infocore.News;
import me.armin.dynamirror.infocore.Quote;
import me.armin.dynamirror.infocore.Sport;
import me.armin.dynamirror.infocore.StockType;
import me.armin.dynamirror.infocore.Stocks;
import me.armin.dynamirror.infocore.Weather;
import me.armin.dynamirror.spotify.TrackInfo;

public class InterfaceController {

	private static String currentTime;
	@FXML
	private Text quote;
	@FXML
	private Text time;
	@FXML
	private Text date;
	@FXML
	private Text music;
	@FXML
	private ProgressBar music_prog;
	@FXML
	private Text temp_c;
	@FXML
	private Text dur;
	@FXML
	private Text prog;
	@FXML
	private ImageView img_currentDay;
	@FXML
	private ImageView img_day2;
	@FXML
	private ImageView img_day3;
	@FXML
	private ImageView img_day4;
	@FXML
	private ImageView img_day5;
	@FXML
	private ImageView img_day6;
	@FXML
	private ImageView img_day7;
	@FXML
	private Text wDay_1;
	@FXML
	private Text wDay_2;
	@FXML
	private Text wDay_3;
	@FXML
	private Text wDay_4;
	@FXML
	private Text wDay_5;
	@FXML
	private Text wDay_6;
	@FXML
	private Text mima1;
	@FXML
	private Text mima2;
	@FXML
	private Text mima3;
	@FXML
	private Text mima4;
	@FXML
	private Text mima5;
	@FXML
	private Text mima6;
	@FXML
	private Text stocks_ssmi;
	@FXML
	private Text stock1;
	@FXML
	private Text stock2;
	@FXML
	private Text stock3;
	@FXML
	private Text ssmi_chng;
	@FXML
	private Text stock1_chng;
	@FXML
	private Text stock2_chng;
	@FXML
	private Text stock3_chng;
	@FXML
	private Text ssmi_perc;
	@FXML
	private Text stock1_perc;
	@FXML
	private Text stock2_perc;
	@FXML
	private Text stock3_perc;
	@FXML
	private Text news1;
	@FXML
	private Text news2;
	@FXML
	private Text news3;
	@FXML
	private Text newsTeaser1;
	@FXML
	private Text newsTeaser2;
	@FXML
	private Text newsTeaser3;
	@FXML
	private Text horoscope;
	@FXML
	private ImageView horoscope_img;
	@FXML
	private Text moon_next;
	@FXML
	private Text moduleL4;
	@FXML
	private Text fa_clothing;
	@FXML
	private Text fa_hair;
	@FXML
	private Text fa_makeup;
	@FXML
	private Text fa_suggestionText;
	@FXML
	private Text fa_rainWarning;
	@FXML
	private Circle fa_color1;
	@FXML
	private Circle fa_color2;
	@FXML
	private AnchorPane ap;
	@FXML
	private AnchorPane fashionAssistantPane;
	@FXML
	private AnchorPane moonPane;
	@FXML
	private AnchorPane stocksPane;
	@FXML
	private AnchorPane horoscopePane;
	@FXML
	private AnchorPane sportPane;
	@FXML
	private Text sport_1st;
	@FXML
	private Text sport_2nd;
	@FXML
	private Text sport_3rd;
	@FXML
	private Text sport_4th;
	@FXML
	private Text sport_5th;
	@FXML
	private Text sport_6th;
	@FXML
	private Text sport_7th;
	@FXML
	private Text sport_8th;
	@FXML
	private Text sport_9th;
	@FXML
	private Text sport_10th;
	@FXML
	private Text sport_11th;
	@FXML
	private Text sport_12th;
	@FXML
	private Text sport_13th;
	@FXML
	private Text sport_14th;
	@FXML
	private Text sport_15th;
	@FXML
	private Text genderIndicator;

	private static Boolean male = false;
	private static Boolean female = false;

	private int b = 1;
	private int sp = 1;

	private static Stage stageBridge;

	public void setStage(Stage stage) {
		stageBridge = stage;
	}

	public static void displayInterface(Gender gender) {
		if (gender == Gender.FEMALE) {
			female = true;
			stageBridge.show();
			stageBridge.toFront();
		} else if (gender == Gender.MALE) {
			male = true;
			stageBridge.show();
			stageBridge.toFront();
		}
		// ic.hideModulesForGender(gender);
	}

	@FXML
	public void init() throws SpotifyWebApiException, IOException {
		moonPane.setVisible(false);
		stocksPane.setVisible(false);
		sportPane.setVisible(false);
		fashionAssistantPane.setVisible(false);
		String[] faInfo = FashionAssistant.getFashionSuggestion();
		fa_suggestionText.setText(faInfo[0]);
		fa_rainWarning.setText(faInfo[1]);
		fa_hair.setText(faInfo[2]);
		fa_color1.setFill(FashionAssistant.getColor1());
		fa_color2.setFill(FashionAssistant.getColor2());
		fa_makeup.setText(faInfo[3]);

		// TODO: Images are only for illustration, currently we have an issue
		// converting the sgv image to an usable png image, sadness noises ):
		Sport.prepareStats();
		String[] f1 = Sport.getCurrentRanking();
		sport_1st.setText(f1[0] + "  " + f1[1] + f1[2] + f1[3]);
		sport_2nd.setText(f1[4] + "  " + f1[5] + f1[6] + f1[7]);
		sport_3rd.setText(f1[8] + "  " + f1[9] + f1[10] + f1[11]);
		sport_4th.setText(f1[12] + "  " + f1[13] + f1[14] + f1[15]);
		sport_5th.setText(f1[16] + "  " + f1[17] + f1[18] + f1[19]);
		sport_6th.setText(f1[20] + "  " + f1[21] + f1[22] + f1[23]);
		sport_7th.setText(f1[24] + "  " +  f1[25] + f1[26] + f1[27]);
		sport_8th.setText(f1[28] + "  " + f1[29] + f1[30] + f1[31]);
		sport_9th.setText(f1[32] + "  " +  f1[33] + f1[34] + f1[35]);
		sport_10th.setText(f1[36] + "  " + f1[37] + f1[38] + f1[39]);
		sport_11th.setText(f1[40] + "  " + f1[41] + f1[42] + f1[43]);
		sport_12th.setText(f1[44] + "  " + f1[45] + f1[46] + f1[47]);
		sport_13th.setText(f1[48] + "  " + f1[49] + f1[50] + f1[51]);
		sport_14th.setText(f1[52] + "  " + f1[53] + f1[54] + f1[55]);
		sport_15th.setText(f1[56] + "  " + f1[57] + f1[58] + f1[59]);

		Image image = new Image("https://i.imgur.com/Ms1Qm5f.png");
		img_currentDay.setImage(image);
		img_day2.setImage(image);
		img_day3.setImage(image);
		img_day4.setImage(image);
		img_day5.setImage(image);
		img_day6.setImage(image);
		img_day7.setImage(image);

		horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.JUNGFRAU));
		horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.JUNGFRAU));
		
		news1.setText(News.getHeadline(0));
		news2.setText(News.getHeadline(1));
		news3.setText(News.getHeadline(2));
		newsTeaser1.setText(News.getTeaserLine(0));
		newsTeaser2.setText(News.getTeaserLine(1));
		newsTeaser3.setText(News.getTeaserLine(2));


		stock1_chng.setText(Stocks.getStock(StockType.CHFEUR).toString());
		stock2_chng.setText(Stocks.getStock(StockType.CHFUSD).toString());
		stock3_chng.setText(Stocks.getStock(StockType.CHFGBP).toString());
		ssmi_chng.setText(Stocks.getStock(StockType.SSMI).toString());

		mima1.setText(Weather.getTemperatureLevelsForDay(2));
		mima2.setText(Weather.getTemperatureLevelsForDay(3));
		mima3.setText(Weather.getTemperatureLevelsForDay(4));
		mima4.setText(Weather.getTemperatureLevelsForDay(5));
		mima5.setText(Weather.getTemperatureLevelsForDay(6));
		mima6.setText(Weather.getTemperatureLevelsForDay(7));

		wDay_1.setText(Weather.getWeekDayForDay(2));
		wDay_2.setText(Weather.getWeekDayForDay(3));
		wDay_3.setText(Weather.getWeekDayForDay(4));
		wDay_4.setText(Weather.getWeekDayForDay(5));
		wDay_5.setText(Weather.getWeekDayForDay(6));
		wDay_6.setText(Weather.getWeekDayForDay(7));
		quote.setText(Quote.getQuote());
		moon_next.setText(Moon.getNextFullMoon());
		{
			final Timeline tlHoroscope = new Timeline(new KeyFrame(Duration.seconds(30), e -> {
				if (b == 1) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.JUNGFRAU));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.JUNGFRAU));
					b++;
				} else if (b == 2) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.ZWILLING));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.ZWILLING));
					b++;
				} else if (b == 3) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.STEINBOCK));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.STEINBOCK));
					b++;
				} else if (b == 4) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.LOEWE));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.LOEWE));
					b++;
				} else if (b == 5) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.FISCH));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.FISCH));
					b++;
				} else if (b == 6) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.KREBS));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.KREBS));
					b++;
				} else if (b == 7) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.SKORPION));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.SKORPION));
					b++;
				} else if (b == 8) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.STIER));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.STIER));
					b++;
				} else if (b == 9) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.WIDDER));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.WIDDER));
					b++;
				} else if (b == 10) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.WASSERMANN));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.WASSERMANN));
					b++;
				} else if (b == 11) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.SCHUETZE));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.SCHUETZE));
					b++;
				} else if (b == 12) {
					horoscope.setText(Horoscope.getHoroscopeForSymbol(HoroscopeSymbol.WAAGE));
					horoscope_img.setImage(Horoscope.getImageForSymbol(HoroscopeSymbol.WAAGE));
					b = 1;
				}

			}));

			tlHoroscope.setCycleCount(Animation.INDEFINITE);
			tlHoroscope.play();
		}
		{
			final Timeline tlNews = new Timeline(new KeyFrame(Duration.hours(1), e -> {
				news1.setText(News.getHeadline(0));
				news2.setText(News.getHeadline(1));
				news3.setText(News.getHeadline(2));
				newsTeaser1.setText(News.getTeaserLine(0));
				newsTeaser2.setText(News.getTeaserLine(1));
				newsTeaser3.setText(News.getTeaserLine(2));

			}));

			tlNews.setCycleCount(Animation.INDEFINITE);
			tlNews.play();
		}
		{
			final Timeline tlStocks = new Timeline(new KeyFrame(Duration.seconds(15), e -> {
				ssmi_chng.setText(Stocks.getStock(StockType.SSMI).toString());
				ssmi_perc.setText(Stocks.getStockPercentage(StockType.SSMI).toString());
				if (sp == 1) {
					sp++;
					stock1_chng.setText(Stocks.getStock(StockType.CHFEUR).toString());
					stock1.setText("CHFEUR");
					stock2_chng.setText(Stocks.getStock(StockType.CHFUSD).toString());
					stock2.setText("CHFUSD");
					stock3_chng.setText(Stocks.getStock(StockType.CHFGBP).toString());
					stock3.setText("CHFGBP");
				} else if (sp == 2) {
					sp++;
					stock1_chng.setText(Stocks.getStock(StockType.AMAZON).toString());
					stock1_perc.setText(Stocks.getStockPercentage(StockType.AMAZON).toString());
					stock1.setText("AMZN");
					stock2_chng.setText(Stocks.getStock(StockType.APPLE).toString());
					stock2_perc.setText(Stocks.getStockPercentage(StockType.APPLE).toString());
					stock2.setText("AAPL");
					stock3_chng.setText(Stocks.getStock(StockType.INTEL).toString());
					stock3_perc.setText(Stocks.getStockPercentage(StockType.INTEL).toString());
					stock3.setText("INTC");

				} else if (sp == 3) {
					sp++;
					stock1_chng.setText(Stocks.getStock(StockType.TESLA).toString());
					stock1.setText("TSLA");
					stock1_perc.setText(Stocks.getStockPercentage(StockType.TESLA).toString());
					stock2_chng.setText(Stocks.getStock(StockType.GOOGLE).toString());
					stock2_perc.setText(Stocks.getStockPercentage(StockType.GOOGLE).toString());
					stock2.setText("GOOGL");
					stock3_chng.setText(Stocks.getStock(StockType.UBS).toString());
					stock3_perc.setText(Stocks.getStockPercentage(StockType.UBS).toString());
					stock3.setText("UBSG");
				} else if (sp == 4) {
					sp++;
					stock1_chng.setText(Stocks.getStock(StockType.BAYER).toString());
					stock1.setText("BAYN");
					stock1_perc.setText(Stocks.getStockPercentage(StockType.BAYER).toString());
					stock2_chng.setText(Stocks.getStock(StockType.GLENCORE).toString());
					stock2_perc.setText(Stocks.getStockPercentage(StockType.GLENCORE).toString());
					stock2.setText("GLEN");
					stock3_chng.setText(Stocks.getStock(StockType.ROCHE).toString());
					stock3_perc.setText(Stocks.getStockPercentage(StockType.ROCHE).toString());
					stock3.setText("ROG");

				} else if (sp == 5) {
					sp = 1;
					stock1_chng.setText(Stocks.getStock(StockType.SIEMENS).toString());
					stock1.setText("SIN");
					stock1_perc.setText(Stocks.getStockPercentage(StockType.SIEMENS).toString());
					stock2_chng.setText(Stocks.getStock(StockType.CS).toString());
					stock2_perc.setText(Stocks.getStockPercentage(StockType.CS).toString());
					stock2.setText("CS");
					stock3_chng.setText(Stocks.getStock(StockType.NESTLE).toString());
					stock3_perc.setText(Stocks.getStockPercentage(StockType.NESTLE).toString());
					stock3.setText("NESN");

				}
			}));

			tlStocks.setCycleCount(Animation.INDEFINITE);
			tlStocks.play();
		}
		{
			final Timeline tlWeather = new Timeline(new KeyFrame(Duration.seconds(3), e -> {
				temp_c.setText(Weather.getTemperatureCurrentDay());
			}));
			tlWeather.setCycleCount(Animation.INDEFINITE);
			tlWeather.play();
		}
		{
			final Timeline tlMusic_prog = new Timeline(new KeyFrame(Duration.seconds(1), e -> {
				try {
					TrackInfo.getInformationAboutUsersCurrentPlayback_Async();
				} catch (SpotifyWebApiException | IOException e1) {

					e1.printStackTrace();
				}
				Double dur_c = Double.parseDouble(TrackInfo.currentTrack.getDurationMs().toString());
				Double prog_c = Double.parseDouble(TrackInfo.livePlayer.getProgress_ms().toString());
				Double perc = (prog_c * 100) / dur_c;
				music_prog.setProgress(perc / 100);

				long dur_m = TimeUnit.MILLISECONDS.toMinutes(TrackInfo.livePlayer.getItem().getDurationMs());
				long dur_s = (TrackInfo.livePlayer.getItem().getDurationMs() / 1000) % 60;
				long prog_m = TimeUnit.MILLISECONDS.toMinutes(TrackInfo.livePlayer.getProgress_ms());
				long prog_s = (TrackInfo.livePlayer.getProgress_ms() / 1000) % 60;
				if (prog_s < 10) {
					prog.setText(prog_m + ":0" + prog_s);
				} else {
					prog.setText(prog_m + ":" + prog_s);
				}
				if (dur_s < 10) {
					dur.setText(dur_m + ":0" + dur_s);
				} else {
					dur.setText(dur_m + ":" + dur_s);
				}
			}));
			tlMusic_prog.setCycleCount(Animation.INDEFINITE);
			tlMusic_prog.play();

		}

		{
			final Timeline tlMusic = new Timeline(new KeyFrame(Duration.seconds(2.5), e -> {
				if (TrackInfo.livePlayer.getIs_playing() != null) {
					music.setText(TrackInfo.currentTrack.getName());

				} else {
					music.setText("Es läuft keine Musik");
				}
			}));
			tlMusic.setCycleCount(Animation.INDEFINITE);
			tlMusic.play();
		}
		{

			final Timeline tlTime = new Timeline(new KeyFrame(Duration.seconds(1), e -> {
				Calendar cal = Calendar.getInstance(TimeZone.getDefault());
				String minutes = "" + cal.get(Calendar.MINUTE);
				String hour = "" + cal.get(Calendar.HOUR_OF_DAY);
				if (minutes.length() == 1) {
					minutes = "0" + minutes;
				}
				if (hour.length() == 1) {
					hour = "0" + hour;
				}
				currentTime = hour + ":" + minutes;
				time.setText(currentTime);
			}));

			tlTime.setCycleCount(Animation.INDEFINITE);
			tlTime.play();
		}
		{
			final Timeline tlDate = new Timeline(new KeyFrame(Duration.seconds(1), e -> {
				Calendar cal = Calendar.getInstance(TimeZone.getDefault());
				String day = "" + cal.get(Calendar.DAY_OF_MONTH);
				int cDay = cal.get(Calendar.DAY_OF_WEEK);
				int cMonth = cal.get(Calendar.MONTH);
				String wDay = "?";
				String month = "?";
				String year = "" + cal.get(Calendar.YEAR);

				switch (cDay) {
				case Calendar.SUNDAY:
					wDay = "Sonntag ";
					break;
				case Calendar.SATURDAY:
					wDay = "Samstag ";
					break;
				case Calendar.FRIDAY:
					wDay = "Freitag ";
					break;
				case Calendar.THURSDAY:
					wDay = "Donnerstag ";
					break;

				case Calendar.WEDNESDAY:
					wDay = "Mittwoch ";
					break;

				case Calendar.TUESDAY:
					wDay = "Dienstag ";
					break;

				case Calendar.MONDAY:
					wDay = "Montag ";
					break;
				}

				switch (cMonth) {
				case Calendar.JANUARY:
					month = "Januar ";
					break;
				case Calendar.FEBRUARY:
					month = "Februar";
					break;
				case Calendar.MARCH:
					month = "März ";
					break;
				case Calendar.APRIL:
					month = "April ";
					break;
				case Calendar.MAY:
					month = "Mai ";
					break;
				case Calendar.JUNE:
					month = "Juni ";
					break;
				case Calendar.JULY:
					month = "Juli ";
					break;
				case Calendar.AUGUST:
					month = "August ";
					break;
				case Calendar.SEPTEMBER:
					month = "September ";
					break;
				case Calendar.OCTOBER:
					month = "Oktober ";
					break;
				case Calendar.NOVEMBER:
					month = "November ";
					break;
				case Calendar.DECEMBER:
					month = "Dezember ";
					break;
				}
				date.setText(wDay + ", " + day + " " + month + year);
			}));
			tlDate.setCycleCount(Animation.INDEFINITE);
			tlDate.play();
			{
				temp_c.setText(Weather.getTemperatureCurrentDay());
				final Timeline temperature = new Timeline(new KeyFrame(Duration.minutes(30), e -> {
					temp_c.setText(Weather.getTemperatureCurrentDay());
				}));
				temperature.setCycleCount(Animation.INDEFINITE);
				temperature.play();
			}
			// TODO: Introduce a smarter AutoSleep with adaptive face detection technology

			{
				final Timeline autoSleep = new Timeline(new KeyFrame(Duration.minutes(1), e -> {
					male = false;
					female = false;
					stageBridge.toBack();
					BioCore.startBiometrics();

				}));
				autoSleep.setCycleCount(Animation.INDEFINITE);
				autoSleep.play();
			}

			{
				final Timeline genderUpdate = new Timeline(new KeyFrame(Duration.seconds(1), e -> {
					if (male.equals(true)) {
						genderIndicator.setText("♂");
						System.out.println("REACHED genderUpdate - male");
						stocksPane.setVisible(true);
						sportPane.setVisible(true);
						moonPane.setVisible(false);
						fashionAssistantPane.setVisible(false);
						male = false;
					} else if (female.equals(true)) {
						genderIndicator.setText("♀");
						System.out.println("REACHED genderUpdate - female");
						stocksPane.setVisible(false);
						sportPane.setVisible(false);
						moonPane.setVisible(true);
						fashionAssistantPane.setVisible(true);
						female = false;

					} else {

					}

				}));
				genderUpdate.setCycleCount(Animation.INDEFINITE);
				genderUpdate.play();
			}
			BioCore.startBiometrics();
			// InterfaceController.displayInterface(Gender.MALE);
		}

	}

}
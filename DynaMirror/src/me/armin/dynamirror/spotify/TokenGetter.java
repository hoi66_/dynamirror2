package me.armin.dynamirror.spotify;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.SpotifyHttpManager;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeRequest;

import java.net.URI;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class TokenGetter {
	private static final String clientId = "468f5871e7f446efbd91f9c9ca624bd7";
	private static final String clientSecret = "e6ae362aa84d41889d38d4aef9da0fca";
	private static final URI redirectUri = SpotifyHttpManager.makeUri("https://example.com/spotify-redirect");
	private static final String code = "AQDZaQzXtWd9KR5NEYAPwuDsBNhphLVd-n-F-yS4QB3EJwOGXCCGdsWLfkkP8zoJMIX6z3N5B5Yqd2N_Qd6JUQ9cDJJvaUNWH-3YvBM36uWHl4RxrjAAf4jmbgZGuY4ZdUTL2ksBCgpdQP9A_eks3xP0kjlwIyuJ6VmE3rUXwPsUmwsPzImVPqdzbAUQJ1PQETe2nw52FDDmF5tgw69zckpMzJO1Zl9z5RAuyFBD8RUw9eDcotf8z9Cf2uMZYrGoSX7uj9ShoeIlj_KMsMkLdja84SPDPLr9TNDSW7xys2boYKXADX8sGQL2La3c79tnMFjT5yGnhd8726fA6ECOqZni5nEk63hKHZkWlCn08bWBjTMeTA";

	private static final SpotifyApi spotifyApi = new SpotifyApi.Builder().setClientId(clientId)
			.setClientSecret(clientSecret).setRedirectUri(redirectUri).build();
	private static final AuthorizationCodeRequest authorizationCodeRequest = spotifyApi.authorizationCode(code).build();
	public static void authorizationCode_Async() {
		try {
			final Future<AuthorizationCodeCredentials> authorizationCodeCredentialsFuture = authorizationCodeRequest
					.executeAsync();

			// ...

			final AuthorizationCodeCredentials authorizationCodeCredentials = authorizationCodeCredentialsFuture.get();

			// Set access and refresh token for further "spotifyApi" object usage
			spotifyApi.setAccessToken(authorizationCodeCredentials.getAccessToken());
			spotifyApi.setRefreshToken(authorizationCodeCredentials.getRefreshToken());

			System.out.println("Expires in: " + authorizationCodeCredentials.getExpiresIn());
			System.out.println("refresh token: " + authorizationCodeCredentials.getRefreshToken());
		} catch (InterruptedException | ExecutionException e) {
			System.out.println("Error: " + e.getCause().getMessage());
		}
	}

}

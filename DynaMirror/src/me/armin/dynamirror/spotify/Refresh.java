package me.armin.dynamirror.spotify;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeRefreshRequest;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Refresh {
	private static final String clientId = "468f5871e7f446efbd91f9c9ca624bd7";
	private static final String clientSecret = "e6ae362aa84d41889d38d4aef9da0fca";
	private static final String refreshToken = "AQDK-9DQFyNJhIZ0dAUXMa3vuclC40KqwolY0ktlZXRi4JUWa_m_kdcJSVQxREPJXlCox3jpmIF6mgoblDpmqH79Gb2qO1LPDsc0n2flGdqSWL7cfCtewW003qP6f09Zixcf6w";
	public static String goldenToken;

	private static final SpotifyApi spotifyApi = new SpotifyApi.Builder().setClientId(clientId)
			.setClientSecret(clientSecret).setRefreshToken(refreshToken).build();
	private static final AuthorizationCodeRefreshRequest authorizationCodeRefreshRequest = spotifyApi
			.authorizationCodeRefresh().build();

	public static void authorizationCodeRefresh_Async() {
		try {
			final Future<AuthorizationCodeCredentials> authorizationCodeCredentialsFuture = authorizationCodeRefreshRequest
					.executeAsync();

			// ...

			final AuthorizationCodeCredentials authorizationCodeCredentials = authorizationCodeCredentialsFuture.get();

			// Set access token for further "spotifyApi" object usage
			spotifyApi.setAccessToken(authorizationCodeCredentials.getAccessToken());

			System.out.println("Expires in: " + authorizationCodeCredentials.getExpiresIn());
			System.out.println("code= " + authorizationCodeCredentials.getAccessToken());
			goldenToken = authorizationCodeCredentials.getAccessToken();
		} catch (InterruptedException | ExecutionException e) {
			System.out.println("Error: " + e.getCause().getMessage());
		}
	}
}

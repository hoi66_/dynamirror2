package me.armin.dynamirror.spotify;

import java.io.IOException;

import com.wrapper.spotify.exceptions.SpotifyWebApiException;

public class SpotifyCore {

	public static void launchSpotifyBridge() throws SpotifyWebApiException, IOException {

		Refresh.authorizationCodeRefresh_Async();
		CurrentTrack.getUsersCurrentlyPlayingTrack_Async();
		TrackInfo.getInformationAboutUsersCurrentPlayback_Async();
	}
}
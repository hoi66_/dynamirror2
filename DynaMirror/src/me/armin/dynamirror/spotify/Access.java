package me.armin.dynamirror.spotify;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.SpotifyHttpManager;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeUriRequest;

import java.net.URI;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Access {
	private static final String clientId = "468f5871e7f446efbd91f9c9ca624bd7";
	private static final String clientSecret = "e6ae362aa84d41889d38d4aef9da0fca";
	private static final URI redirectUri = SpotifyHttpManager.makeUri("https://example.com/spotify-redirect");

	private static final SpotifyApi spotifyApi = new SpotifyApi.Builder().setClientId(clientId)
			.setClientSecret(clientSecret).setRedirectUri(redirectUri).build();
	private static final AuthorizationCodeUriRequest authorizationCodeUriRequest = spotifyApi.authorizationCodeUri()
			.state("x4xkmn9pu3j6ukrs8n")
			.scope("user-read-birthdate,user-read-email,user-read-playback-state,user-modify-playback-state,user-read-currently-playing")
			.show_dialog(true).build();

	public static void authorizationCodeUri_Async() {
		try {
			final Future<URI> uriFuture = authorizationCodeUriRequest.executeAsync();

			// ...

			final URI uri = uriFuture.get();

			System.out.println("URI: " + uri.toString());
		} catch (InterruptedException | ExecutionException e) {
			System.out.println("Error: " + e.getCause().getMessage());
		}
	}
}
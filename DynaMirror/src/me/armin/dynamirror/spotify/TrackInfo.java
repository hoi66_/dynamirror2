package me.armin.dynamirror.spotify;

import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.miscellaneous.CurrentlyPlayingContext;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.requests.data.player.GetInformationAboutUsersCurrentPlaybackRequest;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class TrackInfo {
	public static Track currentTrack;
	public static CurrentlyPlayingContext livePlayer;
	private static final String accessToken = Refresh.goldenToken;

	private static final SpotifyApi spotifyApi = new SpotifyApi.Builder().setAccessToken(accessToken).build();
	private static final GetInformationAboutUsersCurrentPlaybackRequest getInformationAboutUsersCurrentPlaybackRequest = spotifyApi
			.getInformationAboutUsersCurrentPlayback().market(CountryCode.CH).build();
	public static void getInformationAboutUsersCurrentPlayback_Async() throws SpotifyWebApiException, IOException {
		try {
			final Future<CurrentlyPlayingContext> currentlyPlayingContextFuture = getInformationAboutUsersCurrentPlaybackRequest
					.executeAsync();

			// ...
			final CurrentlyPlayingContext currentlyPlayingContext = currentlyPlayingContextFuture.get();
			currentTrack = currentlyPlayingContext.getItem();
			livePlayer = currentlyPlayingContext;
		} catch (InterruptedException | ExecutionException e) {
			System.out.println("Error: " + e.getCause().getMessage());
			livePlayer = null;
			currentTrack = null;
		}
	}
}

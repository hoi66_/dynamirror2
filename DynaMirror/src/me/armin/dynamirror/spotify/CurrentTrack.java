package me.armin.dynamirror.spotify;

import java.io.IOException;

import com.wrapper.spotify.exceptions.SpotifyWebApiException;

import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.model_objects.miscellaneous.CurrentlyPlaying;
import com.wrapper.spotify.requests.data.player.GetUsersCurrentlyPlayingTrackRequest;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class CurrentTrack {
	private static final String accessToken = Refresh.goldenToken;
	private static final SpotifyApi spotifyApi = new SpotifyApi.Builder().setAccessToken(accessToken).build();
	private static final GetUsersCurrentlyPlayingTrackRequest getUsersCurrentlyPlayingTrackRequest = spotifyApi
			.getUsersCurrentlyPlayingTrack().market(CountryCode.CH).build();

	public static void getUsersCurrentlyPlayingTrack_Async() {
		try {
			final Future<CurrentlyPlaying> currentlyPlayingFuture = getUsersCurrentlyPlayingTrackRequest.executeAsync();

			// ...

			final CurrentlyPlaying currentlyPlaying = currentlyPlayingFuture.get();

			// System.out.println("Artist: " + currentlyPlaying.getItem().getName());
		} catch (InterruptedException | ExecutionException e) {
			System.out.println("Error: " + e.getCause().getMessage());
		}
	}
}

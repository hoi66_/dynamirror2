package me.armin.dynamirror.biometric;

import org.bytedeco.javacpp.indexer.Indexer;
import org.bytedeco.javacv.Frame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wrapper.spotify.exceptions.SpotifyWebApiException;

import javafx.application.Platform;
import me.armin.dynamirror.ui.InterfaceController;
import java.io.File;
import java.io.IOException;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_dnn.*;
import static org.bytedeco.javacpp.opencv_imgproc.resize;

import me.armin.dynamirror.infocore.Gender;

public class GenderDetection {
	static int a;

	private static final Logger logger = LoggerFactory.getLogger(GenderDetection.class);

	private Net genderNet;

	public GenderDetection() {

		try {

			genderNet = new Net();

			File protobuf = new File(getClass().getResource("/caffe/deploy_gendernet.prototxt").toURI());

			File caffeModel = new File(getClass().getResource("/caffe/gender_net.caffemodel").toURI());

			Importer importer = createCaffeImporter(protobuf.getAbsolutePath(), caffeModel.getAbsolutePath());

			importer.populateNet(genderNet);

			importer.close();

		} catch (Exception e) {

			logger.error("Error reading prototxt", e);

			throw new IllegalStateException("Unable to start CNNGenderDetector", e);

		}

	}

	/**
	 * 
	 * Predicts gender of a given cropped face
	 *
	 * 
	 * 
	 * @param face  the cropped face as a {@link Mat}
	 * 
	 * @param frame the original frame where the face was cropped from
	 * 
	 * @return Gender
	 * 
	 */

	public Gender predictGender(Mat face, Frame frame) {

		try {

			Mat croppedMat = new Mat();

			resize(face, croppedMat, new Size(256, 256));

			normalize(croppedMat, croppedMat, 0, Math.pow(2, frame.imageDepth), NORM_MINMAX, -1, null);

			Blob inputBlob = new Blob(croppedMat);

			genderNet.setBlob(".data", inputBlob);

			genderNet.forward();

			Blob prob = genderNet.getBlob("prob");

			Indexer indexer = prob.matRefConst().createIndexer();

			if (indexer.getDouble(0, 0) > indexer.getDouble(0, 1) && a < 1) {

				System.out.println("Male detected");
				BioCore.stop();
				makeUIBridge(Gender.MALE);
				return Gender.MALE;

			} else if (!(indexer.getDouble(0, 0) > indexer.getDouble(0, 1)) && a < 1) {

				System.out.println("Female detected");
				BioCore.stop();
				makeUIBridge(Gender.FEMALE);
				return Gender.FEMALE;
			}

		} catch (Exception e) {

			logger.error("Error when processing gender", e);

		}

		return Gender.NOT_RECOGNIZED;

	}

	public static void makeUIBridge(Gender gender) throws SpotifyWebApiException, IOException {

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				InterfaceController.displayInterface(gender);
			}
		});
	}
}
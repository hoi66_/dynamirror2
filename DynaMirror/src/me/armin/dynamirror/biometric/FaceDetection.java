package me.armin.dynamirror.biometric;

import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_imgproc;
import org.bytedeco.javacpp.opencv_objdetect;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static org.bytedeco.javacpp.helper.opencv_objdetect.cvHaarDetectObjects;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING;


public class FaceDetection {

	private opencv_objdetect.CvHaarClassifierCascade haarClassifierCascade;
	private CvMemStorage storage;
	private OpenCVFrameConverter.ToIplImage iplImageConverter;
	private OpenCVFrameConverter.ToMat toMatConverter;

	public FaceDetection() {
		iplImageConverter = new OpenCVFrameConverter.ToIplImage();
		toMatConverter = new OpenCVFrameConverter.ToMat();

		try {
			File haarCascade = new File(
					this.getClass().getResource("/detection/haarcascade_frontalface_alt.xml").toURI());
			haarClassifierCascade = new opencv_objdetect.CvHaarClassifierCascade(cvLoad(haarCascade.getAbsolutePath()));
		} catch (Exception e) {
			throw new IllegalStateException("Error when trying to get the haar cascade", e);
		}
		storage = CvMemStorage.create();
	}

	/**
	 * Detects and returns a map of cropped faces from a given captured frame
	 *
	 * @param frames
	 *            the frame captured by the {@link org.bytedeco.javacv.FrameGrabber}
	 * @return A map of faces along with their coordinates in the frame
	 */
	public Map<CvRect, Mat> detect(Frame frame) {
		Map<CvRect, Mat> detectedFaces = new HashMap<>();
		IplImage iplImage = iplImageConverter.convert(frame);


		/*
		 * return a CV Sequence (kind of a list) with coordinates of rectangle face
		 * area. (returns coordinates of left top corner & right bottom corner)
		 */
		CvSeq detectObjects = cvHaarDetectObjects(rotate(iplImage, -90), haarClassifierCascade, storage, 1.5, 3,
				CV_HAAR_DO_CANNY_PRUNING);
		Mat matImage = toMatConverter.convert(frame);
		int numberOfPeople = detectObjects.total();
		for (int i = 0; i < numberOfPeople; i++) {
			CvRect rect = new CvRect(cvGetSeqElem(detectObjects, i));
			Mat croppedMat = matImage.apply(new Rect(rect.x(), rect.y(), rect.width(), rect.height()));
			detectedFaces.put(rect, croppedMat);
		}

		return detectedFaces;
	}

	public static IplImage rotate(IplImage image, double angle) {
		IplImage copy = opencv_core.cvCloneImage(image);

		IplImage rotatedImage = opencv_core.cvCreateImage(opencv_core.cvGetSize(copy), copy.depth(), copy.nChannels());
		CvMat mapMatrix = opencv_core.cvCreateMat(2, 3, opencv_core.CV_32FC1);

		// Define Mid Point
		CvPoint2D32f centerPoint = new CvPoint2D32f();
		centerPoint.x(copy.width() / 2);
		centerPoint.y(copy.height() / 2);

		// Get Rotational Matrix
		opencv_imgproc.cv2DRotationMatrix(centerPoint, angle, 1.0, mapMatrix);

		// Rotate the Image
		opencv_imgproc.cvWarpAffine(copy, rotatedImage, mapMatrix,
				opencv_imgproc.CV_INTER_CUBIC + opencv_imgproc.CV_WARP_FILL_OUTLIERS, opencv_core.cvScalarAll(170));
		opencv_core.cvReleaseImage(copy);
		opencv_core.cvReleaseMat(mapMatrix);
		return rotatedImage;
	}

	@Override
	public void finalize() {
		cvReleaseMemStorage(storage);
	}
}

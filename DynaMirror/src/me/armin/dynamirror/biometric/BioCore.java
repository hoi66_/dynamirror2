package me.armin.dynamirror.biometric;

import org.bytedeco.javacpp.avutil;
import org.bytedeco.javacpp.opencv_core.CvRect;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameGrabber;
import me.armin.dynamirror.sound.SOUNDS;
import me.armin.dynamirror.sound.SoundManager;
import java.io.IOException;
import java.util.Map;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;


public class BioCore implements Runnable {

	private static OpenCVFrameGrabber frameGrabber;
	private volatile static boolean running = false;

	private static FaceDetection faceDetector = new FaceDetection();
	private static GenderDetection genderDetector = new GenderDetection();

	public BioCore() {

	}


	private static void process() {
		running = true;
		while (running) {
			try {
				// Here we grab frames from our camera
				final Frame frame = frameGrabber.grab();

				Map<CvRect, Mat> detectedFaces = faceDetector.detect(frame);

				detectedFaces.entrySet().forEach(rectMatEntry -> {
					try {
						SoundManager.playSound(SOUNDS.BOOTING);
					} catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					genderDetector.predictGender(rectMatEntry.getValue(), frame);
				});

			} catch (FrameGrabber.Exception e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void stop() {
		running = false;
		try {
			frameGrabber.release();
			frameGrabber.stop();
		} catch (FrameGrabber.Exception e) {
			e.printStackTrace();
		}
	}

	public static void startBiometrics() {
		Runnable runnable = new BioCore(); 
		Thread thread = new Thread(runnable);
		thread.setDaemon(true);
		thread.start();
	}

	public void run() {
		frameGrabber = new OpenCVFrameGrabber(1);
		frameGrabber.setVideoCodec(13);
		frameGrabber.setFormat("dshow");
		frameGrabber.setVideoBitrate(10 * 1024 * 1024);
		frameGrabber.setFrameRate(30.0);
		frameGrabber.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);
		frameGrabber.setImageWidth(1280);
		frameGrabber.setVideoOption("preset", "ultrafast");
		frameGrabber.setImageHeight(720);

		try {
			frameGrabber.start();
		} catch (FrameGrabber.Exception e) {
			throw new RuntimeException("Unable to start the FrameGrabber", e);
		}

		process();
	}
}